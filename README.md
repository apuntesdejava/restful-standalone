# Servidor RESTful sin contenedor Java EE: Grizzly, Jersey y Maven #

¿Quieres implementar un servidor RESTful sin usar GlassFish, JBoss, Tomcat, Wildfly, Payara, Jetty, WebLogic ni nada parecido? ¿y en Java sin usar Node.js?

Bueno, aquí les comparto una manera de montar de montar un servidor RESTful usando Jersey pero ejecutable desde la línea de comandos.

* [Vídeo](https://youtu.be/GTyXugZNVoA)
* [Post en el blog](http://www.apuntesdejava.com/2016/03/servidor-restful-sin-contenedor-java-ee.html)