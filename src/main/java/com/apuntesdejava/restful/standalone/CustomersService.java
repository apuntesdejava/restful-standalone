/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apuntesdejava.restful.standalone;

import com.apuntesdejava.restful.standalone.domain.Customer;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author dsilva
 */
@Path("customers")
public class CustomersService {

    private static final Logger LOG = Logger.getLogger(CustomersService.class.getName());

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("all")
    public Response findAll() {
        EntityManager em = getEntityManager();
        TypedQuery<Customer> query = em.createNamedQuery("Customer.findAll", Customer.class);
        List<Customer> list = query.getResultList();
        return Response.ok(list).build();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu");
        return emf.createEntityManager();
    }

    @GET
    @Produces(MediaType.TEXT_XML)
    @Path("{id}")
    public Response findById(@PathParam("id") int id) {
        try {
            EntityManager em = getEntityManager();
            Customer customer = em.find(Customer.class, id);
            return Response.ok(customer).build();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return Response.serverError().build();
    }
}
